terraform {
	required_providers {
		aws = {
			source = "hashicorp/aws"
			version = "~> 3.25"
		}
		aviatrix = {
			source = "AviatrixSystems/aviatrix"
			version = "~> 2.18.0"
		}
	}
}

provider "aws" {
	alias = "use1"
	region = "us-east-1"
	access_key = var.aws_access_key
	secret_key = var.aws_secret_key
}

provider "aws" {
	alias = "usw1"
	region = "us-west-1"
	access_key = var.aws_access_key
	secret_key = var.aws_secret_key
}

provider "aviatrix" {
  
}

data "aviatrix_transit_gateway" "demo_transit" {
  gw_name = "Test-Transit"
}

data "aviatrix_transit_gateway" "demo_transit_west" {
  gw_name = "TestWest-Transit"
}

module "ezCSREast1" {
  source 					= "./ezCSR"
  providers					= { aws = aws.use1 }
  hostname					= "CSROnprem-East1"
  public_key				= "ssh-rsa acdefg"
  vpc_cidr					= "172.16.0.0/16"
  public_sub				= "172.16.0.0/24"
  private_sub				= "172.16.1.0/24"
  instance_type				= "t2.medium"
  avtx_gateways				= [ data.aviatrix_transit_gateway.demo_transit, data.aviatrix_transit_gateway.demo_transit_west ]
  avtx_gw_bgp_as_num		= [ "64525", "64526" ]
  csr_bgp_as_num			= "64527"
  create_client				= true
}

module "ezCSRWest1" {
  source 					= "./ezCSR"
  providers					= { aws = aws.usw1 }
  hostname					= "CSROnprem-West2"
  public_key				= "ssh-rsa abcdefg"
  vpc_cidr					= "172.24.0.0/16"
  public_sub				= "172.24.0.0/24"
  private_sub				= "172.24.1.0/24"
  instance_type				= "t2.medium"
  avtx_gateways				= [ data.aviatrix_transit_gateway.demo_transit, data.aviatrix_transit_gateway.demo_transit_west ]
  avtx_gw_bgp_as_num		= [ "64525", "64526" ]
  csr_bgp_as_num			= "64528"
  create_client				= true
}
